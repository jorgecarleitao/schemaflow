[![Python version](https://img.shields.io/pypi/v/schemaflow.svg?longCache=true&style=flat-square)](https://pypi.org/pypi/schemaflow/)
[![pipeline status](https://gitlab.com/jorgecarleitao/schemaflow/badges/master/pipeline.svg)](https://gitlab.com/jorgecarleitao/schemaflow/commits/master)
[![Coverage Status](https://gitlab.com/jorgecarleitao/schemaflow/badges/master/coverage.svg)](https://gitlab.com/jorgecarleitao/schemaflow/-/jobs/artifacts/master/file/htmlcov/index.html?job=coverage)

# SchemaFlow

This is a a package to write data pipelines for data science systematically in Python 2 and 3.
Thanks for checking it out.

Check out the very comprehensive documentation [here](https://jorgecarleitao.gitlab.io/schemaflow/documentation/latest/).

## The problem that this package solves

A major challenge in creating a robust data pipeline is guaranteeing interoperability between
pipes: how do we guarantee that the pipe that someone wrote is compatible
with others' pipe *without* running the whole pipeline multiple times until we get it right?

## The solution that this package adopts
 
This package declares an API to define a stateful data transformation that gives 
the developer the opportunity to declare what comes in, what comes out, and what states are modified
on each pipe and therefore the whole pipeline. Check out 
[`tests/test_pipeline.py`](https://gitlab.com/jorgecarleitao/schemaflow/blob/master/tests/test_pipeline.py) or 
[`examples/end_to_end_kaggle.py`](https://gitlab.com/jorgecarleitao/schemaflow/blob/master/examples/end_to_end_kaggle.py)

## Install 

    pip install schemaflow

or, install the latest (recommended for now):

    git clone git@gitlab.com:jorgecarleitao/schemaflow.git
    cd schemaflow && pip install -e .

## Run examples

We provide one example that demonstrate the usage of SchemaFlow's API
on developing an end-to-end pipeline applied to 
[one of Kaggle's exercises](https://www.kaggle.com/c/house-prices-advanced-regression-techniques).

To run it, download the data in that exercise to `examples/all/` and run

    pip install -r examples/requirements.txt
    python examples/end_to_end_kaggle.py

You should see some prints to the console as well as the generation of 3 files at 
`examples/`: two plots and one `submission.txt`.

## Run tests

    pip install -r tests/requirements.txt
    python -m unittest discover

## Build documentation

    pip install -r docs/requirements.txt
    cd docs && make html && cd ..
    open docs/build/html/index.html
